<?php include_once __DIR__ .'/vendor/autoload.php';

use app\models\Recipe;

$hits = null;

if($search != null){
    $result = Recipe::search($search);
    $hits = $result['hits']['total'];
}?>

<div class="container">
    <div class="row" id="result-title">
        <?php if($hits != null):?>
            <strong class="pull-right">Found <?=$hits?> recipes just for you</strong>
        <?php endif?>

        <?php if($search != null):?>
            <h4> <i class="glyphicon glyphicon-search"></i> <?=$search?></h4>
        <?php endif?>
    </div>

    <?php if($hits != null){
        foreach($result['hits']['hits'] as $recipeData){
            $recipe = $recipeData['_source']?>
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title col-md-12 pull-left">
                        <a href="<?=$recipe['url']?>"><?=$recipe['name']?></a>
                    </h3>
                    <span class="pull-right"><i class="glyphicon glyphicon-stats"> <?=$recipeData['_score']?></i></span>
                </div>
                <div class="panel-body">
                    <div class="col-md-4 center-block">
                        <?php if(isset($recipe['image']) && $recipe['image'] != null){?>
                        <img src="<?=$recipe['image']?>" class="img-responsive image"
                             onerror="this.src='assets/img/comingsoon.jpg'">
                    <?php }else{?>
                        <img src="assets/img/comingsoon.jpg" class="img-responsive image">
                    <?php }?>
                    </div>

                    <div class="col-md-8">
                        <?php if(isset($recipe['cookTime']) || isset($recipe['prepTime'])):?>
                        <div class="row">
                            <?php if(isset($recipe['cookTime'])):?>
                            <i class="col-md-4"><span class="glyphicon glyphicon-time"></span> <?=$recipe['cookTime']?> to cook </i>
                            <?php endif;?>
                            <?php if(isset($recipe['prepTime'])):?>
                            <i class="col-md-4"><span class="glyphicon glyphicon-time"></span> <?=$recipe['prepTime']?> to prepare </i>
                            <?php endif?>
                            <?php if(isset($recipe['recipeYield'])):?>
                            <i class="col-md-4 pull-right"><span class="glyphicon glyphicon-cutlery"></span> <?=$recipe['recipeYield']?> </i>
                            <?php endif?>
                            <hr class="col-md-12">
                        </div>
                        <?php endif?>

                        <?php if(isset($recipe['ingredients'])):?>
                        <div class="row">
                            <h5 class="heading">Ingredients</h5>
                            <p><?=$recipe['ingredients']?></p>
                        </div>
                        <?php endif?>

                        <div class="row">
                            <h5 class="heading">Description</h5>
                            <p><?=$recipe['description']?></p>
                        </div>
                        <a class="pull-right" href="<?=$recipe['url']?>"><?=$recipe['source']?></a>
                    </div>
                </div>
            </div>
    <?php }}?>
</div>


