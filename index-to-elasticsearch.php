<?php

$filePath = __DIR__ . '/data/recipeitems-latest.json';

if(!file_exists($filePath)){
    echo "Please download the dataset from
            http://openrecipes.s3.amazonaws.com/recipeitems-latest.json.gz,
            extract it and place the json file in {root}/data/recipeitems-latest.json. Then try again";
    exit;
}

if(php_sapi_name() !== 'cli'){
    echo 'long operation. please run in console mode';
    exit;
}

$handle = @fopen($filePath , "r");
$index_count = 0;

if ($handle){
    require_once __DIR__ . '/vendor/autoload.php';

    while (($buffer = fgets($handle, 4096)) !== false) {
        $object = json_decode($buffer);
        if($object != null){
            $recipe = new app\models\Recipe($object);
            $recipe->index();
            $index_count++;
        }
    }

    if (!feof($handle))
        echo "Error: unexpected fgets() fail\n";

    fclose($handle);
}
echo "finished indexing $index_count recipes";