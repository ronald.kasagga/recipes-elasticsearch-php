<?php

namespace app\models;

/**
 * @property string $id
 * @property string $name
 * @property string $ingredients
 * @property string $url
 * @property string $image
 * @property string $ts_date
 * @property string $cookTime
 * @property string $source
 * @property string $recipeYield
 * @property string $datePublished
 * @property string $prepTime
 * @property string $description
 */
class Recipe extends Model
{
    public function __construct($object)
    {
        parent::__construct('recipe');
        $this->setAttributes($object);
    }

    private function setAttributes($json_object)
    {
        $this->id = $json_object->_id->{'$oid'};
        $this->ts_date = $json_object->ts->{'$date'};

        if(isset($json_object->datePublished))
            $this->datePublished = date_create($json_object->datePublished)->format('Y-m-d');

        if(isset($json_object->cookTime))
            $this->cookTime = self::parseRecipeTime($json_object->cookTime);

        if(isset($json_object->prepTime))
            $this->prepTime = self::parseRecipeTime($json_object->prepTime);

        foreach (get_object_vars($json_object) as $attribute => $value){
            if(!in_array($attribute, ['_id', 'ts', 'cookTime', 'prepTime', 'datePublished']))
                $this->$attribute = $value;
        }
    }

    private static function parseRecipeTime($recipeTime)
    {
        $recipeTime = str_replace('PT', ' ', $recipeTime);
        $recipeTime = str_replace('M', ' Minutes ', $recipeTime);
        $recipeTime = str_replace('H', ' Hours ', $recipeTime);
        return trim($recipeTime);
    }

    public static function search($searchParams, $mode='multi_match'){
        return parent::search($searchParams, $mode, [
            'multi_match'=>['name', 'ingredients', 'cookTime', 'prepTime', 'recipeYield', 'source']]);
    }
}