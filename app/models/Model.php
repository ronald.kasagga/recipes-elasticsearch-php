<?php

namespace app\models;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

/**
 * @property string $type
 * @property Client client
 * @property string $id
 */
class Model
{
    const ELASTIC_SEARCH_INDEX = 'accessmobile';
    private $type;
    private static $client;

    public function __construct($type)
    {
        $this->type = $type;
    }

    public function index(){
        $params = [
            'index' => self::ELASTIC_SEARCH_INDEX,
            'type' => $this->type,
            'id' => $this->id,
            'body' => $this->getAttributes(),
        ];

        return self::getClient()->index($params);
    }

    private static function getClient(){
        if(self::$client == null)
            self::$client = ClientBuilder::create()->build();

        return self::$client;
    }

    public function getAttributes()
    {
        $attributes = get_object_vars($this);
        unset($attributes['type']);

        return $attributes;
    }

    public static function search($searchParams, $mode='multi_match', $options = [])
    {
        $params = [
            'index' => self::ELASTIC_SEARCH_INDEX,
            'type' => (new self(''))->type,
            'body' => [
                'query'=>[]
            ],
        ];

        switch($mode){
            case 'multi_match';
                $params['body']['query'][$mode] = [
                    'query'=>$searchParams,
                    'fields'=>$options[$mode],
                ];
                break;
        }

        return self::getClient()->search($params);
    }
}