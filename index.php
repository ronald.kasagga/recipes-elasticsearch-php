<?php $search = isset($_REQUEST['search']) ? $_REQUEST['search'] : null ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Recipes Search</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/bootstrap/3.3.6/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron text-center">
                <h1>Recipes</h1>
                <form method="post">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="input-group input-group-lg">
                            <input type="search" name='search' value="<?=$search?>"
                                   class="form-control" placeholder="... 15 minute lunch...">
                            <span class="input-group-btn">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="glyphicon glyphicon-cutlery"></i> Search
                                </button>
                            </span>
                        </div>
                    </div>
                </form>
                <br><br>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div id="search-results" class="container">
            <?php include_once __DIR__ .'/search.php'?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="text-muted text-center">
                &copy; <span>devs@<b style="color: grey">access</b>&bull;<b style="color: cornflowerblue">mobile</b></span> <?=date('Y')?>
            </p>
        </div>
    </footer>
</div> <!-- /container -->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script>window.jQuery || document.write('<script src="assets/jquery/jquery-1.12.1.min.js"><\/script>')</script>
<script src="assets/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</body>
</html>
